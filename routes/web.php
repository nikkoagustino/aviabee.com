<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CombinedController;
use App\Http\Controllers\RepaintController;
use App\Http\Controllers\SceneryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ToolsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CombinedController::class, 'index']);
Route::post('search', [CombinedController::class, 'search']);
Route::get('sitemap', [CombinedController::class, 'sitemap']);
Route::get('sitemap.xml', [CombinedController::class, 'xml']);
Route::get('refreshFrontpage', [CombinedController::class, 'refreshFrontpage']);

Route::get('scenery', [SceneryController::class, 'index']);
Route::get('scenery/{url_slug}', [SceneryController::class, 'showSingleScenery']);
Route::get('scenery/version/{fs_version}', [SceneryController::class, 'getByCategory']);

Route::get('repaint', [RepaintController::class, 'index']);
Route::get('repaint/{url_slug}', [RepaintController::class, 'showSingleRepaint']);
Route::get('repaint/aircraft/{aircraft_type}', [RepaintController::class, 'getByAircraftType']);
Route::get('repaint/airline/{airline_icao}', [RepaintController::class, 'getByAirline']);
Route::get('repaint/version/{fs_version}', [RepaintController::class, 'getByFSVersion']);

Route::get('tools', [ToolsController::class, 'index']);
Route::get('tools/{url_slug}', [ToolsController::class, 'single']);

Route::get('user/login', [UserController::class, 'login']);