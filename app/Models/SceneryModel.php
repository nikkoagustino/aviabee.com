<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class SceneryModel extends Model
{
    use HasFactory;

    static function getAllScenery($fs_version)
    {
        if ($fs_version == 'all')
        {
            $all_scenery = DB::table('scenery')->paginate(5);
        }
        else
        {
            $all_scenery = DB::table('scenery')->where('fs_version', $fs_version)->paginate(5);
        }
        return $all_scenery;
    }

    static function getAllNoPaginate()
    {
        $data = DB::table('scenery')->get();
        return $data;
    }

    static function getScenery($url_slug)
    {
        $scenery = DB::table('scenery')->where('url_slug', $url_slug)->get();
        return $scenery;
    }
}
