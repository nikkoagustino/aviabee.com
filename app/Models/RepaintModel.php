<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class RepaintModel extends Model
{
    use HasFactory;

    
    static function getAllRepaint()
    {
        $data = DB::table('repaint')
                    ->join('aircraft', 'aircraft_type', '=', 'aircraft.aircraft_type_id')
                    ->join('airline', 'airline_icao', '=', 'airline.icao')
                    ->join('user', 'post_user_id', '=', 'user.user_id')
                    ->select('manufacturer', 'aircraft_type', 'variant', 'airline_icao', 'airline_iata', 'airline_name', 'logo_url', 'screenshot_url', 'model_developer', 'fs_version', 'registration', 'texture', 'texture_variation', 'notes', 'download_link', 'post_time', 'fullname')
                    ->paginate(5);
        return $data;
    }

    static function getAllNoPaginate()
    {
        $data = DB::table('repaint')
                    ->join('aircraft', 'aircraft_type', '=', 'aircraft.aircraft_type_id')
                    ->join('airline', 'airline_icao', '=', 'airline.icao')
                    ->join('user', 'post_user_id', '=', 'user.user_id')
                    ->select('manufacturer', 'aircraft_type', 'variant', 'airline_icao', 'airline_iata', 'airline_name', 'logo_url', 'screenshot_url', 'model_developer', 'fs_version', 'registration', 'texture', 'texture_variation', 'notes', 'download_link', 'post_time', 'fullname')
                    ->get();
        return $data;
    }

    static function getAllForSitemap()
    {
        $data = DB::table('repaint')
                    ->join('aircraft', 'aircraft_type', '=', 'aircraft.aircraft_type_id')
                    ->join('airline', 'airline_icao', '=', 'airline.icao')
                    ->join('user', 'post_user_id', '=', 'user.user_id')
                    ->select('manufacturer', 'aircraft_type', 'variant', 'airline_icao', 'airline_iata', 'airline_name', 'logo_url', 'screenshot_url', 'model_developer', 'fs_version', 'registration', 'texture', 'texture_variation', 'notes', 'download_link', 'post_time', 'fullname')
                    ->orderBy('fs_version', 'ASC')
                    ->orderBy('model_developer', 'ASC')
                    ->orderBy('variant', 'ASC')
                    ->orderBy('registration', 'ASC')
                    ->get();
        return $data;
    }

    static function getRepaintByFSVersion($fs_version)
    {
        $data = DB::table('repaint')
                    ->join('aircraft', 'aircraft_type', '=', 'aircraft.aircraft_type_id')
                    ->join('airline', 'airline_icao', '=', 'airline.icao')
                    ->join('user', 'post_user_id', '=', 'user.user_id')
                    ->select('manufacturer', 'aircraft_type', 'variant', 'airline_icao', 'airline_iata', 'airline_name', 'logo_url', 'screenshot_url', 'model_developer', 'fs_version', 'registration', 'texture', 'texture_variation', 'notes', 'download_link', 'post_time', 'fullname')
                    ->where('fs_version', $fs_version)
                    ->paginate(5);
        return $data;
    }
    
    static function getRepaintByAirline($airline_icao)
    {
        $data = DB::table('repaint')
                    ->join('aircraft', 'aircraft_type', '=', 'aircraft.aircraft_type_id')
                    ->join('airline', 'airline_icao', '=', 'airline.icao')
                    ->join('user', 'post_user_id', '=', 'user.user_id')
                    ->select('manufacturer', 'aircraft_type', 'variant', 'airline_icao', 'airline_iata', 'airline_name', 'logo_url', 'screenshot_url', 'model_developer', 'fs_version', 'registration', 'texture', 'texture_variation', 'notes', 'download_link', 'post_time', 'fullname')
                    ->where('airline_icao', $airline_icao)
                    ->paginate(5);
        return $data;
    }
    
    static function getRepaintByAircraftType($aircraft_type)
    {
        $data = DB::table('repaint')
                    ->join('aircraft', 'aircraft_type', '=', 'aircraft.aircraft_type_id')
                    ->join('airline', 'airline_icao', '=', 'airline.icao')
                    ->join('user', 'post_user_id', '=', 'user.user_id')
                    ->select('manufacturer', 'aircraft_type', 'variant', 'airline_icao', 'airline_iata', 'airline_name', 'logo_url', 'screenshot_url', 'model_developer', 'fs_version', 'registration', 'texture', 'texture_variation', 'notes', 'download_link', 'post_time', 'fullname')
                    ->where('aircraft_type', $aircraft_type)
                    ->paginate(5);
        return $data;
    }
    
    static function getRepaint($fs_version, $model_developer, $registration)
    {
        $data = DB::table('repaint')
                    ->join('aircraft', 'aircraft_type', '=', 'aircraft.aircraft_type_id')
                    ->join('airline', 'airline_icao', '=', 'airline.icao')
                    ->join('user', 'post_user_id', '=', 'user.user_id')
                    ->select('manufacturer', 'aircraft_type', 'variant', 'airline_icao', 'airline_iata', 'airline_name', 'logo_url', 'screenshot_url', 'model_developer', 'fs_version', 'registration', 'texture', 'texture_variation', 'notes', 'download_link', 'post_time', 'fullname')
                    ->where('model_developer', 'like', $model_developer)
                    ->where('fs_version', 'like', $fs_version)
                    ->where('registration', 'like', $registration)
                    ->get();
        return $data;
    }
}
