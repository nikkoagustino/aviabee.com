<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class CombinedModel extends Model
{
    use HasFactory;

    static function getAll()
    {
        $data = DB::table('frontpage')
                    ->orderBy('post_time', 'desc')
                    ->paginate(10);
        return $data;
    }

    static function getAllNoPaginate()
    {
        $data = DB::table('frontpage')
                    ->get();
        return $data;
    }


    static function refreshFrontpage($l1, $l2, $l3, $url, $img, $time)
    {

        DB::insert('INSERT INTO frontpage (first_line, second_line, third_line, url_slug, screenshot_url, post_time) VALUES (?, ?, ?, ?, ?, ?)', array($l1, $l2, $l3, $url, $img, $time));
    }
}
