<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class HelperModel extends Model
{
    use HasFactory;

    static function random()
    {
        $data = DB::table('frontpage')
                    ->orderBy(DB::raw('RAND()'))
                    ->take(6)
                    ->get();
        return $data;
    }
}
