<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ToolsModel extends Model
{
    use HasFactory;

    static function getAll()
    {
        $data = DB::table('tools')->paginate(5);
        return $data;
    }
    
    static function getAllNoPaginate()
    {
        $data = DB::table('tools')->get();
        return $data;
    }

    static function getSingle($name, $fs_version)
    {
        if (isset($fs_version))
        {
            $data = DB::table('tools')
                        ->where("title", "like", $name)
                        ->where("fs_version", "like", $fs_version)
                        ->get();
        }
        else
        {
            $data = DB::table('tools')
                        ->where("title", "like", $name)
                        ->get();
        }
        return $data;
    }
}
