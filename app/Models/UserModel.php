<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserModel extends Model
{
    use HasFactory;

    static function getCredentials($username) {
        $data = DB::table('user')
                    ->where('user_id', '=', $username)
                    ->get();
        return $data;
    }
}
