<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CombinedModel as Model;
use App\Models\RepaintModel;
use App\Models\SceneryModel;
use App\Models\ToolsModel;


class CombinedController extends Controller {

    public function index()
    {
        $data = [
            'data' => Model::getAll(),
            ];
        return view('index')->with($data);
    }

    public function sitemap()
    {
        $repaintLink = null;
        $sceneryLink = null;
        $toolsLink = null;
        $repaintData = RepaintModel::getAllForSitemap();
        foreach ($repaintData as $row) {
            $url = "http://".$_SERVER['HTTP_HOST']."/repaint/".strtolower($row->fs_version."_".str_replace(" ", "-", $row->model_developer)."_".$row->registration);
            $repaintLink .= '<a href="'.$url.'">'.$row->fs_version.' - '.$row->model_developer.' '.$row->manufacturer.' '.$row->variant.' '.$row->airline_name.' '.$row->texture_variation.' '.$row->registration.'</a>';
        }
        $sceneryData = SceneryModel::getAllNoPaginate();
        foreach ($sceneryData as $row) {
            $url = "http://".$_SERVER['HTTP_HOST']."/scenery/".$row->url_slug;
            $sceneryLink .= '<a href="'.$url.'">'.$row->fs_version.' - '.$row->airport_icao.' '.$row->airport_name.', '.$row->airport_location.', '.$row->country.'</a>';
        }
        $toolsData = ToolsModel::getAllNoPaginate();
        foreach ($toolsData as $row) {
            if (isset($row->fs_version)){
                $link = $row->fs_version." - ".$row->title;
            } else {
                $link = $row->title;
            }
            $url = "http://".$_SERVER['HTTP_HOST']."/tools/".str_replace("+", "_", urlencode(strtolower($link)));
            if (isset($row->fs_version)){
                $toolsLink .= '<a href="'.$url.'">'.$row->fs_version.' - '.$row->title .'</a>';
            } else {
                $toolsLink .= '<a href="'.$url.'">FS2004/FSX - '.$row->title .'</a>';
            }
        }
        $data = [
            'repaintLink' => $repaintLink,
            'sceneryLink' => $sceneryLink,
            'toolsLink' => $toolsLink,
        ];
        return view('index')->with($data);
    }

    public function xml()
    {
        header("Content-type: application/xml");
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        <url><loc>http://aviabee.com</loc><changefreq>daily</changefreq><priority>0.2</priority></url>';
        $repaintData = RepaintModel::getAllNoPaginate();
        foreach ($repaintData as $row) {
            $url = "http://".$_SERVER['HTTP_HOST']."/repaint/".strtolower($row->fs_version."_".str_replace(" ", "-", $row->model_developer)."_".$row->registration);
            $xml .= '<url><loc>'.$url.'</loc><priority>1.0</priority></url>';
        }
        $sceneryData = SceneryModel::getAllNoPaginate();
        foreach ($sceneryData as $row) {
            $url = "http://".$_SERVER['HTTP_HOST']."/scenery/".$row->url_slug;
            $xml .= '<url><loc>'.$url.'</loc><priority>1.0</priority></url>';
        }
        $toolsData = ToolsModel::getAllNoPaginate();
        foreach ($toolsData as $row) {
            if (isset($row->fs_version)){
                $link = $row->fs_version." - ".$row->title;
            } else {
                $link = $row->title;
            }
            $url = "http://".$_SERVER['HTTP_HOST']."/tools/".str_replace("+", "_", urlencode(strtolower($link)));
            $xml .= '<url><loc>'.$url.'</loc><priority>1.0</priority></url>';
        }
        $xml .= '</urlset>';
        echo $xml;
    }

    public function refreshFrontpage()
    {
        $repaintLink = null;
        $sceneryLink = null;
        $toolsLink = null;
        $repaintData = RepaintModel::getAllNoPaginate();
        $count = 0;
        foreach ($repaintData as $row) {
            $url = "http://".$_SERVER['HTTP_HOST']."/repaint/".strtolower($row->fs_version."_".str_replace(" ", "-", $row->model_developer)."_".$row->registration);
            Model::refreshFrontpage($row->fs_version." - ".$row->model_developer, $row->manufacturer.' '.$row->variant, $row->airline_name.' '.$row->registration, $url, $row->screenshot_url, $row->post_time);
        }
        $sceneryData = SceneryModel::getAllNoPaginate();
        foreach ($sceneryData as $row) {
            $url = "http://".$_SERVER['HTTP_HOST']."/scenery/".$row->url_slug;
            Model::refreshFrontpage($row->fs_version.' - '.$row->airport_icao.' / '.$row->airport_iata.' '.$row->airport_name, $row->airport_location.', '.$row->country, 'Scenery by '.$row->author, $url, $row->screenshot_url, $row->post_time);
        }
        $toolsData = ToolsModel::getAllNoPaginate();
        foreach ($toolsData as $row) {
            if (isset($row->fs_version)){
                $link = $row->fs_version." - ".$row->title;
            } else {
                $link = $row->title;
            }
            $first_line_description = explode("\r\n", $row->description);
            $url = "http://".$_SERVER['HTTP_HOST']."/tools/".str_replace("+", "_", urlencode(strtolower($link)));
            Model::refreshFrontpage($link, $first_line_description[0], '', $url, $row->screenshot_url, $row->post_time);
        }
    }

    public function search(Request $request)
    {
        $search_query = explode(" ", $request->input('search_query'));
        $dataFull = Model::getAllNoPaginate();
        $search_result = [];
        foreach ($dataFull as $key => $value) {
            $i = 0;
            $isMatch = true;
            while ($i < count($search_query))
            {
                if ($isMatch == true)
                {
                    if (($this->isLike($value->first_line, $search_query[$i])) || ($this->isLike($value->second_line, $search_query[$i])) || ($this->isLike($value->third_line, $search_query[$i])))
                    {
                        $isMatch = true;
                    }
                    else
                    {
                        $isMatch = false;
                    }
                }
                ++$i;
            }
            if ($isMatch == true)
            {
                array_push($search_result, $value);
            }
        }
        $data = [
            'result' => $search_result,
            'query' => $request->input('search_query')
            ];
        return view('index')->with($data);
    }

    public function isLike($string, $needle)
    {
        if ((strpos(strtolower($string), strtolower($needle)) === 0) || (strpos(strtolower($string), strtolower($needle)) > 0)) {
            return true;
        } else {
            return false;
        }
    }
}