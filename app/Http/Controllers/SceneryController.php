<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SceneryModel as Model;

class SceneryController extends Controller {

    public function index()
    {
        $data = [
            'data' => Model::getAllScenery('all')
            ];
        return view('index')->with($data);
    }
    
    public function getByCategory($fs_version)
    {
        $data = [
            'data' => Model::getAllScenery($fs_version)
            ];
        return view('index')->with($data);
    }
    
    public function showSingleScenery($url_slug)
    {
        $dataRow = Model::getScenery($url_slug);
        foreach ($dataRow as $key => $value) {
            $title = $value->fs_version." ".$value->airport_icao."/".$value->airport_iata." ".$value->airport_name.", ".$value->airport_location;
        }
        $data = [
            'data' => $dataRow,
            'title' => $title
            ];
        return view('index')->with($data);
    }

}
