<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\RepaintModel as Model;

class RepaintController extends Controller {

    public function index()
    {
        $data = [
            'data' => Model::getAllRepaint(),
            ];
        return view('index')->with($data);
    }
    
    public function getByFSVersion($fs_version)
    {
        $data = [
            'data' => Model::getRepaintByFSVersion($fs_version),
            ];
        return view('index')->with($data);
    }
    
    public function getByAirline($airline_icao)
    {
        $data = [
            'data' => Model::getRepaintByAirline($airline_icao),
            ];
        return view('index')->with($data);
    }
    
    public function getByAircraftType($aircraft_type)
    {
        $data = [
            'data' => Model::getRepaintByAircraftType($aircraft_type),
            ];
        return view('index')->with($data);
    }
    
    public function showSingleRepaint($url_slug)
    {
        $array_criteria = explode("_", $url_slug);
        $fs_version = strtoupper($array_criteria[0]);
        $model_developer = str_replace("-", " ", $array_criteria[1]);
        $registration = strtoupper($array_criteria[2]);
        
        $dataRow = Model::getRepaint($fs_version, $model_developer, $registration);
        foreach ($dataRow as $key => $value) {
            $title = $value->fs_version." ".$value->model_developer." ".$value->manufacturer." ".$value->variant." ".$value->registration;
        }
        $data = [
            'data' => $dataRow,
            'title' => $title
            ];
        return view('index')->with($data);
    }
}
