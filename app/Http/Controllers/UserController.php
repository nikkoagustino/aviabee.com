<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel as Model;

class UserController extends Controller {

    function login(Request $request) 
    {
        $username = $request->input('username');
        $plainPass = $request->input('password');
        $userdata = Model::getCredentials($username);
        if (count($userdata) > 0)
        {
            foreach ($userdata as $key => $value) 
            {
                $salt = $value->salt;
                $encrypted = $value->password;
            }
            
            $md5password = md5($plainPass.$salt);
            if ($md5password === $encrypted)
            {
                echo "login";
            }
            else
            {
                echo "wrong password";
            }
        }
        else
        {
            echo "user not found";
        }
        
    }

}