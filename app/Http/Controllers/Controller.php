<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct()
    {
        $GLOBALS['randomPost'] = null;
        $data = \App\Models\HelperModel::random();
        foreach ($data as $key => $value) {
            $genString = $value->first_line.' '.$value->second_line;
            if (strpos($value->third_line, 'Scenery') !== 0){
                $genString .= ' '.$value->third_line;
            }
            // if (strlen($genString) > 45) {
            //  $genString = substr($genString, 0, 42).'...';
            // }
            $GLOBALS['randomPost'] .= '<span><a href="'.$value->url_slug.'">'.strip_tags($genString).'</a></span>';
        }
    }
}
