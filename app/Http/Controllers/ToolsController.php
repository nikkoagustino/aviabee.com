<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ToolsModel as Model;

class ToolsController extends Controller {

    public function index()
    {
        $data = [
            'data' => Model::getAll(),
            ];
        return view('index')->with($data);
    }

    public function single($url)
    {
        if (strpos($url, "-") > 0)
        {
            $request_uri = explode("_-_", $url);
            $fs_version = $request_uri[0];
            $tools_name = str_replace("_", " ", $request_uri[1]);
        }
        else
        {
            $fs_version = null;
            $tools_name = str_replace("_", " ", $url);
        }

        $data = [
            'data' => Model::getSingle($tools_name, $fs_version),
            'title' => ucwords($tools_name)
            ];
        return view('index')->with($data);
    }
}