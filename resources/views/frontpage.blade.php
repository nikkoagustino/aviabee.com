			@foreach ($data as $row)
		<div class="alert alert-transparent">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<img src="{{ ($row->screenshot_url) ? $row->screenshot_url : url('assets/uploads/no_img.png'); }}" alt="" class="screenshot">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
					<table>
						<tr>
							<td><?= $row->first_line ?></td>
						</tr>
						<tr>
							<td><?= $row->second_line ?></td>
						</tr>
						<tr>
							<td><?= $row->third_line ?></td>
						</tr>
					</table>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" onclick="window.location.replace('{{ $row->url_slug }}')">
					<div class="btn btn-warning btn-download">Read More and Download</div>
				</div>
			</div>
		</div>
		@endforeach