<?php $uri_segment = explode("/", Request::path()); if ($uri_segment[0] == '') $uri_segment[0] = 'Home'; ?>
<!DOCTYPE html>
<html>
<head>
	<title><?= (isset($title)) ? $title : ucfirst($uri_segment[0]); ?> - Aviabee</title>
	<meta name="google-site-verification" content="1fYsD8D9pcAalFvrvTlqhtPox7BvjWR4wtYeri5AiwQ" />
	<meta name="robots" content="index, follow">
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.4/cosmo/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/aviabee.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/icon-aviabee.min.css') }}">
	<link rel="icon" type="image/x-icon" href="{{ url('assets/img/favicon.ico') }}">
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js" async="async"></script>
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" async="async"></script>
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="container">
	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 sidebar">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-md-12 col-xs-12 nopadding">
				<div class="btn btn-success">
					<a href="{{ url('/') }}"><img src="{{ url('assets/img/logo.png') }}" alt="Aviabee Logo"></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-md-12 col-xs-12 nopadding">
				<div class="btn btn-primary search-wrapper">
					<form action="{{ url('/') }}/search" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="text" class="form-control" name="search_query" placeholder="Type Your Search Keyword Here..." required>
						<button type="submit" class="btn btn-success btn-search"><span class="icon-search"></span></button>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 nopadding">
				<a href="{{ url('/') }}/repaint/version/fs2004">
					<div class="btn btn-danger">FS2004 Repaint
						<span class="icon-repaint icon-menu"></span>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 nopadding">
				<a href="{{ url('/') }}/scenery/version/fs2004">
					<div class="btn btn-success">FS2004 Scenery
						<span class="icon-scenery icon-menu"></span>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 nopadding">
				<a href="{{ url('/') }}/repaint/version/fsx">
					<div class="btn btn-warning">FSX Repaint
						<span class="icon-repaint icon-menu"></span>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 nopadding">
				<a href="{{ url('/') }}/scenery/version/fsx">
					<div class="btn btn-info">FSX Scenery
						<span class="icon-scenery icon-menu"></span>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 nopadding">
				<a href="{{ url('/') }}/tools">
					<div class="btn btn-warning">Tools
						<span class="icon-tools icon-menu"></span>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 nopadding">
				<a href="{{ url('/') }}/sitemap">
					<div class="btn btn-danger">Sitemap
						<span class="icon-sitemap icon-menu"></span>
					</div>
				</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 nopadding">
				<div class="btn btn-success">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 down10 nopadding">
						<span class="icon-calendar icon-menu"></span>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 down10 nopadding">
						<table>
							<tr>
								<td>UTC Time</td>
							</tr>
							<tr class="calendar">
								<?php date_default_timezone_set("UTC"); ?>
								<td><?= date("d M Y") ?></td>
							</tr>
							<tr class="calendar">
								<td><b><?= date("H : i", time())." UTC" ?></b></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
				<div class="btn btn-primary nocursor">
					<div class="randomPost"><i class="icon-random"></i> Random Post</div>
					<?= $GLOBALS['randomPost'] ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  nopadding">
				<div class="btn btn-warning btn-copyright">Copyright &copy; 2012-<?= date("Y") ?> Aviabee | Macintel Aviation | AndromedaX<br>
				idbeetech Web Development
				</div>
			</div>
		</div>
	</div>

	{{-- Content Body --}}
	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
	<div style="margin-top: 2px"></div>
		{{-- Loop --}}
		@if ($uri_segment[0] == 'repaint')
			@include('repaint')
		@elseif ($uri_segment[0] == 'scenery')
			@include('scenery')
		@elseif ($uri_segment[0] == 'tools')
			@include('tools')
		@elseif ($uri_segment[0] == 'sitemap')
			@include('sitemap')
		@elseif ($uri_segment[0] == 'search')
			@if (count($result) == 0)
				<div class="alert alert-danger">No Result for "{{ $query }}"</div>
			@else
				@include('search')
			@endif
		@else
			@include('frontpage')
		@endif
		{{-- End Loop --}}
		
		@if (isset($data) && (count($data) > 1))
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				{{ $data->links("pagination::bootstrap-4") }}
			</div>
		</div>
		@elseif (isset($data) && (count($data) == 0))
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="alert alert-warning"><center><h3>There is no post in this category.</h3></center></div>
			</div>
		</div>
		@endif
	</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63122659-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>