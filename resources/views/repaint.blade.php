			@foreach ($data as $row)
			<div class="alert alert-transparent">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php $url = strtolower($row->fs_version."_".str_replace(" ", "-", $row->model_developer)."_".$row->registration); ?>
					<span class="title"><a href="/repaint/{{ $url }}">{{ $row->fs_version }} - {{ $row->model_developer }} {{ $row->manufacturer }} {{ $row->variant }} {{ $row->airline_name }} {{ $row->texture_variation }} {{ $row->registration }}</a></span>
					<div class="separator"></div>
					<span class="info">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding"><i class="icon-user">{{ $row->fullname }}</i></div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 nopadding"><i class="icon-calendar">{{ date("d M Y", strtotime($row->post_time)) }}</i></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding"><a href="/repaint/airline/{{ $row->airline_icao }}"><i class="icon-ticket">{{ $row->airline_name }}</i></a></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding"><a href="/repaint/aircraft/{{ $row->aircraft_type }}"><i class="icon-flight">{{ $row->manufacturer }} {{ $row->variant }}</i></a></div>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="down10"></div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
					<img src="{{ ($row->screenshot_url) ? $row->screenshot_url : url('assets/uploads/no_img.png'); }}" alt="" class="screenshot">
				</div>
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
					<table>
						<tr>
							<td>Developer</td>
							<td>{{ $row->model_developer }}</td>
						</tr>
						<tr>
							<td>Platform</td>
							<td>
								@if (strtolower($row->fs_version) == 'fsx')
									Microsoft Flight Simulator X
								@elseif (strtolower($row->fs_version) == 'fs2004')
									Microsoft Flight Simulator 2004
								@elseif (strtolower($row->fs_version) == 'xplane')
									X-Plane
								@elseif (strtolower($row->fs_version) == 'p3d')
									Lockheed Martin Prepar3D
								@endif
							</td>
						</tr>
						<tr>
							<td>Aircraft Type</td>
							<td><a href="/repaint/aircraft/{{ $row->aircraft_type }}">{{ $row->manufacturer }} {{ $row->variant }}</a></td>
						</tr>
						<tr>
							<td>Airline</td>
							<td><a href="/repaint/airline/{{ $row->airline_icao }}">{{ $row->airline_name }}</a></td>
						</tr>
						<tr>
							<td>Registration</td>
							<td>{{ $row->registration }}</td>
						</tr>
						<tr>
							<td>Texture</td>
							<td>{{ $row->texture }}</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				@if ($row->notes)
				<div class="down10"></div>
				<div class="alert alert-danger alert-notes">Note: {{ $row->notes }}</div>
				@endif
				<div class="down10"></div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"></div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="btn btn-warning btn-download" onclick="window.open('{{ $row->download_link }}', '_blank')">
						<span class="icon-download"></span>
						Download Now
					</div>
				</div>
			</div>
		</div>
			@endforeach
