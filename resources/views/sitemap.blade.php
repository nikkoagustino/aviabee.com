		<div class="alert alert-transparent">
			<div class="row">
				<div class="col-lg-12 col-md-12" id="sitemapWrapper">
					<div class="btn btn-danger btn-download">REPAINT</div>
					<?= $repaintLink ?>
					<div class="down10"></div>
					<div class="btn btn-success btn-download">SCENERY</div>
					<?= $sceneryLink ?>
					<div class="down10"></div>
					<div class="btn btn-warning btn-download">TOOLS</div>
					<?= $toolsLink ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
				<div class="down10"></div>
				<div class="down10"></div>
					<a class="pull-right" href="/sitemap.xml">XML Sitemap</a>
				</div>
			</div>
		</div>
		<style type="text/css">
			#sitemapWrapper a {
				display: block;
				padding-left: 30px;
			}
		</style>