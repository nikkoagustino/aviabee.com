			@foreach ($data as $row)
		<div class="alert alert-transparent">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<span class="title"><a href="/scenery/{{ $row->url_slug }}">{{ $row->fs_version }} - {{ $row->airport_icao }} {{ $row->airport_name }}, {{ $row->airport_location }}, {{ $row->country }}</a></span>
					<div class="separator"></div>
					<span class="info">
						<div class="col-lg-4 col-md-4"><i class="icon-user">{{ $row->author }}</i></div>
						<div class="col-lg-3 col-md-3"><i class="icon-world">{{ $row->country }}</i></div>
						<div class="col-lg-5 col-md-5"><i class="icon-location">{{ $row->airport_location }}</i></div>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="down10"></div>
				<div class="col-lg-5 col-md-5">
					<img src="{{ ($row->screenshot_url) ? $row->screenshot_url : url('assets/uploads/no_img.png'); }}" alt="" class="screenshot">
				</div>
				<div class="col-lg-7 col-md-7">
					<table>
						<tr>
							<td>Author</td>
							<td>{{ $row->author }}</td>
						</tr>
						<tr>
							<td>Platform</td>
							<td>{{ $row->fs_version }}</td>
						</tr>
						<tr>
							<td>Location</td>
							<td>{{ $row->airport_location }}</td>
						</tr>
						<tr>
							<td>Country</td>
							<td>{{ $row->country }}</td>
						</tr>
						<tr>
							<td>ICAO</td>
							<td>{{ $row->airport_icao }}</td>
						</tr>
						<tr>
							<td>IATA</td>
							<td>{{ $row->airport_iata }}</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				@if ($row->notes)
				<div class="down10"></div>
				<div class="alert alert-danger alert-notes">Note: {{ $row->notes }}</div>
				@endif
				<div class="down10"></div>
				<div class="col-lg-8 col-md-8"></div>
				<div class="col-lg-4 col-md-4">
					<div class="btn btn-warning btn-download" onclick="window.open('{{ $row->download_link }}', '_blank')">
						<span class="icon-download"></span>
						Download Now
					</div>
					@if ($row->download_fix)
					<div class="btn btn-success btn-download" onclick="window.open('{{ $row->download_fix }}', '_blank')">
						<span class="icon-download"></span>
						Download: {{ $row->fix_name }}
					</div>
					@endif
				</div>
			</div>
		</div>
		@endforeach