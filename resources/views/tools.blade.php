			@foreach ($data as $row)
			<?php 
			if (isset($row->fs_version)){
				$link = $row->fs_version." - ".$row->title;
			} else {
				$link = $row->title;
			}
			$url = str_replace("+", "_", urlencode(strtolower($link)));
			$hash = substr(md5($url), 0, 5); ?>
		<div class="alert alert-transparent">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<span class="title"><a href="/tools/{{ $url }}">{{ $row->fs_version or 'FS2004/FSX' }} - {{ $row->title }}</a></span>
					<div class="separator"></div>
				</div>
			</div>
			<div class="row">
				<div class="down10"></div>
				<div class="col-lg-5 col-md-5">
					<img src="{{ ($row->screenshot_url) ? $row->screenshot_url : url('assets/uploads/no_img.png'); }}" alt="" class="screenshot">
				</div>
				<div class="col-lg-7 col-md-7">
					<?php echo nl2br($row->description) ?>
				</div>
			</div>
			<div class="row">
				<div class="down10"></div>
				<div class="col-lg-8 col-md-8">
					<div class="btn btn-primary btn-download" onclick="showChangelog('{{ $hash }}')" id="show_{{ $hash }}">
						<span class="icon-info"></span>
						Show Changelog
					</div>
					<div class="btn btn-danger btn-download" onclick="hideChangelog('{{ $hash }}')" id="hide_{{ $hash }}" style="display:none">
						<span class="icon-info"></span>
						Hide Changelog
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="btn btn-warning btn-download" onclick="window.open('{{ $row->download_link }}', '_blank')">
						<span class="icon-download"></span>
						Download Now
					</div>
				</div>
			</div>
			<div class="row" id="changelog_{{ $hash }}" hidden>
				<div class="col-lg-12 col-md-12">
					<div class="down10"></div>
					<?php $change = json_decode($row->changelog); ?>
					@foreach ($change as $rows)
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">Version: {{ $rows->version }}</h3>
						</div>
						<div class="panel-body">
							<?= $rows->changelog ?>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endforeach

<script type="text/javascript">
	function showChangelog(hash) {
		$("#changelog_"+hash).show();
		$("#show_"+hash).hide();
		$("#hide_"+hash).show();
	}
	function hideChangelog(hash) {
		$("#changelog_"+hash).hide();
		$("#show_"+hash).show();
		$("#hide_"+hash).hide();
	}
</script>

	<?php if (isset($uri_segment[1])): ?>
		<script type="text/javascript">
		$(document).ready(function(){
			showChangelog('{{$hash}}');
		});
		</script>
	<?php endif; ?>